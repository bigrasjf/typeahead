# Typeahead exercise
Simple Typeahead engine implementation using concurrent ternary search tree.

## Introduction
The application returns the first 10 movie records matching a word prefix.
Movie records are indexed by title in a ternary search tree.
The implementation aims to be thread-safe and it can answer queries while processing input files and updating the tree.

## Usage
Interaction with the application is done using sdtin/stdout. Commands are read from stdin and results are printed on stdout.

### Commands

#### query
Returns the first 10 results that match the specified prefix. This is case insensitive.

Syntax: `query PREFIX`

Example:
~~~~
query da
1964    UK    A Hard Day's Night
1955    US    Bad Day at Black Rock
1980    US    Coal Miner's Daughter
1985    UK    Dance With a Stranger
~~~~

#### process-file
Asynchronously parse the specified file as movie records and update the typeahead engine with the new records.
The input file is in plain text format and contains one movie record per line. 
A movie record has the following format: `[Release Year]\t[Country Code]\t[Title]`

Syntax: `process-file FILE`

Example:
~~~~
process-file movies1.txt
~~~~

#### quit
Leave the application

Syntax: `quit`


## Compatibility
This project is compatible with Java SE 1.8 or newer. All dependencies are handled by Maven.

## Build instructions
~~~~
mvn clean install
~~~~

## Run the application
~~~~
java -jar typeahead-0.0.1.jar
~~~~
