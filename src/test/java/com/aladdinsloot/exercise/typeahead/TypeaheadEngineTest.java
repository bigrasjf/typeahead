package com.aladdinsloot.exercise.typeahead;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

import junit.framework.TestCase;

public class TypeaheadEngineTest extends TestCase {
	private final static int ENTRIES = 50000;
	private Path filename;
	private TypeaheadEngine typeahead;
	private MovieRecord[] records;

	public TypeaheadEngineTest(String name) {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		typeahead = new TypeaheadEngine();
		filename = Files.createTempFile("typeahead", ".test");
		records = new MovieRecord[ENTRIES];

		final ThreadLocalRandom rnd = ThreadLocalRandom.current();

		// Generate a file with random titles
		try (BufferedWriter writer = 
				Files.newBufferedWriter(filename, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
			for (int i = 0; i < ENTRIES; i++) {
				records[i] = new MovieRecord(rnd.nextInt(1900, 2017), "US", RandomStringUtils.nextString(50, 100));
				writer.write(records[i].toString());
				writer.newLine();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	protected void tearDown() throws Exception {
		typeahead.close();
		records = null;
		Files.deleteIfExists(filename);
	}

	public void testProcessFile() {
		try {
			final Future<?> future = typeahead.parseFile(filename);

			// Wait for completion and check the values
			try {
				future.get();
				for (int i = 0; i < ENTRIES; i++) {
					final Collection<MovieRecord> c = typeahead.query(records[i].getTitle());
					assertTrue(c.contains(records[i]));
				}
			} catch (Exception e) {
				fail("Exception waiting for future results: " + e.getMessage());
			}
		} catch (IOException e) {
			fail("Exception parsing input file " + e.getMessage());
		}
	}
}
