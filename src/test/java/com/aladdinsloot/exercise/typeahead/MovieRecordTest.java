package com.aladdinsloot.exercise.typeahead;

import junit.framework.TestCase;

public class MovieRecordTest extends TestCase {
	private final String title = "The Silence of the Lambs";
	private final int year = 1991;
	private final String country = "US";

	public MovieRecordTest(String name) {
		super(name);
	}

	public void testParse1() {
		final String text = year + "\t" + country + "\t" + title;
		final MovieRecord rec = MovieRecord.parse(text);

		assertEquals(title, rec.getTitle());
		assertEquals(year, rec.getYear());
		assertEquals(country, rec.getCountryCode());
	}

	public void testParse2() {
		final String text = "\t" + year + "\t\t" + country + "\t\t" + title + "\t\t";
		final MovieRecord rec = MovieRecord.parse(text);

		assertEquals(title, rec.getTitle());
		assertEquals(year, rec.getYear());
		assertEquals(country, rec.getCountryCode());
	}

	public void testParse3() {
		final String text = year + "\t" + country + "\t";

		try {
			final MovieRecord rec = MovieRecord.parse(text);
			fail("Should not reach here !");
			System.out.println(rec.toString());
		} catch (Exception e) { }
	}

	public void testCompareLess() {
		final MovieRecord rec1 = new MovieRecord(1900, "US", "a-movie");
		final MovieRecord rec2 = new MovieRecord(1900, "US", "b-movie");

		assertTrue(rec1.compareTo(rec2) < 0);
		assertFalse(rec1.equals(rec2));
		assertTrue(rec1.hashCode() != rec2.hashCode());
	}

	public void testCompareEqual() {
		final MovieRecord rec1 = new MovieRecord(1900, "US", "c-movie");
		final MovieRecord rec2 = new MovieRecord(1900, "US", "c-movie");

		assertTrue(rec1.compareTo(rec2) == 0);
		assertTrue(rec1.equals(rec2));
		assertTrue(rec1.hashCode() == rec2.hashCode());
	}
}
