package com.aladdinsloot.exercise.typeahead;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import junit.framework.TestCase;

public class MultiTernaryTreeTest extends TestCase {

	private MultiTernaryTree<Object> tree;
	
	public MultiTernaryTreeTest(String name) {
		super(name);
	}

	@Override
	protected void setUp() throws Exception {
		tree = new MultiTernaryTree<Object>();
	}

	@Override
	protected void tearDown() throws Exception {
		tree = null;
	}

	public void testInsertNull() {
		final Object o1 = new Object();
		try {
			tree.put("hello", null);
			fail("Should not reach here");
		} catch (NullPointerException e) { }
		
		try {
			tree.put("", o1);
			fail("Should not reach here");
		} catch (Exception e) { }
		
		try {
			tree.put(null, o1);
			fail("Should not reach here");
		} catch (NullPointerException e) { }
	}
	
	public void testInsert1() {
		final Object o1 = new Object();
		tree.put("hello", o1);
		
		Collection<Object> c = tree.get("hello");
		assertTrue(c.size() == 1);
		
		final Iterator<Object> it = c.iterator();
		assertTrue(it.hasNext());
		assertTrue(o1.equals(it.next()));
	}
	
	public void testInsert2() {
		final Object o1 = new Object();
		tree.put("hello world", o1);
		
		Collection<Object> c = tree.get("hello");
		assertTrue(c.size() == 0);
	}
	
	public void testInsertParallel() {
		final int size = 10000;
		final String[] indexes = new String[size];
		final Object[] objects = new Object[size];
		
		for (int i = 0; i < size; i++) {
			indexes[i] = RandomStringUtils.nextString(100, 200);
			objects[i] = new Object();
		}
		
		IntStream.range(0, size).parallel().forEach((val) -> {
			this.tree.put(indexes[val],objects[val]);
		});
		
		IntStream.range(0, size).parallel().forEach((val) -> {
			Collection<Object> c = tree.get(indexes[val]);
			assertTrue(!c.isEmpty());
			assertTrue(c.contains(objects[val]));
		});
	}
	
	public void testCollect1() {
		final String index = "hello world !";
		final Object o1 = new MovieRecord(1900, "XX", index);
		final Object o2 = new MovieRecord(1900, "XX", index);
		
		tree.put(index, o1);
		tree.put(index, o2);
		
		Collection<Object> c1 = tree.collectWithPrefix(index, Collectors.toCollection(TreeSet::new));
		assertTrue(c1.size() == 1);
		
		Collection<Object> c2 = tree.collectWithPrefix(index, Collectors.toCollection(HashSet::new));
		assertTrue(c2.size() == 1);
	}
	
	public void testCollect2() {
		final String index = "hello world !";
		final Object o1 = new MovieRecord(1900, "XX", index);
		final Object o2 = new MovieRecord(1900, "XX", index);
		
		tree.put(index, o1);
		tree.put(index, o2);
		
		Collection<Object> c1 = tree.collectWithPrefix("bonjour", Collectors.toCollection(TreeSet::new));
		assertTrue(c1.size() == 0);
		
		Collection<Object> c2 = tree.collectWithPrefix("hello", Collectors.toCollection(TreeSet::new));
		assertTrue(c2.size() == 1);
	}
}
