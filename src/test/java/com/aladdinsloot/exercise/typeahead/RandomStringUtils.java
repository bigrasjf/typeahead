package com.aladdinsloot.exercise.typeahead;

import java.util.concurrent.ThreadLocalRandom;

public final class RandomStringUtils {
	private final static String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'\"!@#$%^&*(),.? ";
	private RandomStringUtils() { }

	public static String nextString(int length) {
		if (length <= 0) {
			return "";
		}
		final ThreadLocalRandom current = ThreadLocalRandom.current();
		final char[] buffer = new char[length];
		for (int i = 0; i < length; i++) {
			buffer[i] = chars.charAt(current.nextInt(chars.length()));
		}
		return new String(buffer);
	}

	public static String nextString(int min, int max) {
		final ThreadLocalRandom rnd = ThreadLocalRandom.current();
		final int len = (min <= max) ? rnd.nextInt(min, max+1) : rnd.nextInt(max, min+1);
		return nextString(len);
	}
}
