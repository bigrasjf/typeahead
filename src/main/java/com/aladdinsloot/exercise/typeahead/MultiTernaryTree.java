package com.aladdinsloot.exercise.typeahead;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collector;

public class MultiTernaryTree<T> {
	private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
	private final Lock r = rwl.readLock();
	private final Lock w = rwl.writeLock();
	private Node<T> root;

	public MultiTernaryTree() {

	}

	/**
	 * Insert a new key/value into the tree.
	 * @param key
	 * @param val
	 */
	public void put(String key, T val) {
		if (val == null || key == null) {
			throw new NullPointerException();
		}
		if (key.isEmpty()) {
			throw new IllegalArgumentException("Specified key is empty");
		}

		w.lock();
		try {
			root = put(root, key, val, 0);
		} finally {
			w.unlock();
		}
	}

	private Node<T> put(Node<T> x, String key, T val, int pos) {
		final char c = key.charAt(pos);
		if (x == null) {
			x = new Node<T>(c);
		}
		if (c < x.ch) {
			x.left = put(x.left,  key, val, pos);
		}
		else if (c > x.ch) { 
			x.right = put(x.right, key, val, pos);
		}
		else if (pos + 1 < key.length())  { 
			x.center = put(x.center, key, val, pos+1);
		}
		else {
			if (x.values == null) {
				x.values = new LinkedList<T>();
			}
			x.values.add(val);
		}
		return x;
	}

	/**
	 * Retrieve all values associated with a key
	 * @param key
	 * @return
	 */
	public Collection<T> get(String key) {
		if (key == null) {
			throw new NullPointerException();
		}
		if (key.isEmpty()) {
			throw new IllegalArgumentException("Specified key is empty");
		}

		r.lock();
		try {
			final Node<T> x = get(root, key, 0);
			return (x == null || x.values == null) ? Collections.emptyList() : Collections.unmodifiableList(x.values);
		} finally {
			r.unlock();
		}
	}

	private Node<T> get(final Node<T> x, String key, int pos) {
		if (x == null) {
			return null;
		}
		final char c = key.charAt(pos);
		if (c < x.ch) {
			return get(x.left, key, pos);
		}
		else if (c > x.ch) {
			return get(x.right, key, pos);
		}
		else if (pos + 1 < key.length()) {
			return get(x.center, key, pos+1);
		}
		return x;
	}

	/**
	 * Collect all values where key prefix is matching the specified prefix
	 * @param prefix
	 * @param collector
	 * @return
	 */
	public <A,R> R collectWithPrefix(String prefix, Collector<T,A,R> collector) {
		final A accumulator = collector.supplier().get();
		r.lock();

		try {
			final Node<T> x = get(root, prefix, 0);
			if (x != null) {
				if (x.values != null) {
					x.values.forEach((v) -> collector.accumulator().accept(accumulator, v));
				}
				collectValues(x.center, collector, accumulator);
			}
		} finally {
			r.unlock();
		}
		return collector.finisher().apply(accumulator);
	}

	// Collect all values in that subtree
	private <A,R> void collectValues(Node<T> x, Collector<T,A,R> collector, A accumulator) {
		if (x != null) {
			collectValues(x.left, collector, accumulator);
			if (x.values != null) {
				x.values.forEach((v) -> collector.accumulator().accept(accumulator, v));
			}
			collectValues(x.center, collector, accumulator);
			collectValues(x.right, collector, accumulator);
		}
	}

	/**
	 * Internal structure used by ternary tree
	 *
	 * @param <T>
	 */
	private static final class Node<T> {
		private final char ch;
		private Node<T> left, center, right;
		private List<T> values;

		private Node(char ch) {
			this.ch = ch;
		}
	}
}
