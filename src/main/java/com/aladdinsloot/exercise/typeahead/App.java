package com.aladdinsloot.exercise.typeahead;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Scanner;

public class App implements Runnable  {
	private final static String COMMAND_QUIT = "quit";
	private final static String COMMAND_QUERY = "query";
	private final static String COMMAND_PROCESS = "process-file";
	private final static int QUERY_RESULT_LIMIT = 10;

	private final TypeaheadEngine typeahead = new TypeaheadEngine();
	
	public App() {
		
	}
	
	/**
	 * Process word prefix query and print results on stdout
	 * @param prefix
	 */
	private void processQuery(String prefix) {
		final Collection<MovieRecord> c = this.typeahead.query(prefix);
		c.stream().limit(QUERY_RESULT_LIMIT).forEach(System.out::println);
	}
	
	private void processFile(String filename) {
		try {
			this.typeahead.parseFile(Paths.get(filename));
		} catch (Exception e) {
			System.err.println("Exception processing input file " + filename + ": " + e.getMessage());
		}
	}
	
	/**
	 * Main application loop that reads commands from stdin
	 */
	@Override
	public void run() {
		final Scanner scanner = new Scanner(System.in);
		boolean run = true;
		try {
			String cmd = "";
			do {
				cmd = scanner.nextLine();

				// Print the command
				System.out.println(cmd);

				// Processing
				final String[] parts = cmd.split("\\s+");
				if (parts.length > 0) {
					switch (parts[0]) {
					case COMMAND_QUIT:
						run = false;
						break;
					case COMMAND_QUERY:
						if (parts.length >= 2) {
							processQuery(StringUtils.arrayToString(parts, 1, parts.length, " "));
						} else {
							System.err.println("ERROR: Missing option for query command");
						}
						break;
					case COMMAND_PROCESS:
						if (parts.length >= 2) {
							processFile(parts[1]);
						} else {
							System.err.println("ERROR: Missing option for process-file command");
						}
						break;
					default:
						break;
					}
				}
			} while (run && scanner.hasNextLine());
		} catch (Exception e) {
			System.err.println("ERROR Exception processing line: " + e.getMessage());
		} finally {
			scanner.close();
			try {
				this.typeahead.close();
			} catch (IOException e) { 
				System.err.println("ERROR: Exception closing typeahead engine: " + e.getMessage());
			}
		}
	}

	/**
	 * Application entry point
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new App().run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
