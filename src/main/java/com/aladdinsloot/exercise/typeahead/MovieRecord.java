package com.aladdinsloot.exercise.typeahead;

import java.util.Objects;

public class MovieRecord implements Comparable<MovieRecord> {
	private final int year;
	private final String countryCode;
	private final String title;
	
	public MovieRecord(int year, String countryCode, String title) {
		this.year = year;
		this.countryCode = countryCode.trim();
		this.title = Objects.requireNonNull(title).trim();
	}

	/**
	 * Parse and return a new MovieRecord
	 * Expected input format is: [Release year]\t[Country Code]\t[Title]
	 * 
	 * @param text
	 * @return
	 */
	public static MovieRecord parse(CharSequence text) {
		if (text == null) {
			throw new NullPointerException();
		}
		final String[] toks = text.toString().trim().split("\\t+", 3);
		if (toks.length != 3) {
			throw new IllegalArgumentException("Invalid input string");
		}
		return new MovieRecord(Integer.parseInt(toks[0].trim()), toks[1], toks[2]);
	}
	
	public int getYear() {
		return year;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getTitle() {
		return title;
	}
	
	@Override
	public String toString() {
		return year + "\t" + countryCode + "\t" + title;
	}

	@Override
	public int compareTo(MovieRecord o) {
		int cmp = this.title.compareTo(o.title);
		if (cmp == 0) {
			cmp = this.countryCode.compareTo(o.countryCode);
			if (cmp == 0) {
				cmp = Integer.compare(this.year, o.year);
			}
		}
		return cmp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieRecord other = (MovieRecord) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
}
