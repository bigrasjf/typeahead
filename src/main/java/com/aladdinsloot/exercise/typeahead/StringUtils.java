package com.aladdinsloot.exercise.typeahead;

public final class StringUtils {

	private StringUtils() { }
	
	/**
	 * Utility function to concatenate an Array into a String with range [start, end)
	 * @param arr
	 * @param start
	 * @param end
	 * @param separator
	 * @return
	 */
	public static String arrayToString(String[] arr, int start, int end, String separator) {
		if (arr == null)
			throw new NullPointerException();
		if (end > arr.length || start < 0 || start >= end)
			throw new IllegalArgumentException("Invalid start/end boundaries");
		
		final StringBuilder b = new StringBuilder(arr[start]);
		start++;
		
		for (; start < end; start++) {
			b.append(separator).append(arr[start]);
		}
		return b.toString();
	}
}
