package com.aladdinsloot.exercise.typeahead;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class TypeaheadEngine implements Closeable {
	private final ExecutorService executor;
	private final MultiTernaryTree<MovieRecord> tree = new MultiTernaryTree<MovieRecord>();

	public TypeaheadEngine(ExecutorService executor) {
		this.executor = executor;
	}

	public TypeaheadEngine() {
		this(Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors() + 1)));
	}

	public Future<?> parseFile(Path filename) throws IOException {
		if (filename == null) {
			throw new NullPointerException();
		}
		if (!Files.isReadable(filename)) {
			throw new IOException("Input file is not readable: " + filename.toString());
		}
		return this.executor.submit(() -> {
			try (BufferedReader reader = Files.newBufferedReader(filename)) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					try {
						final MovieRecord rec = MovieRecord.parse(line);
						indexRecord(rec);
					} catch (Exception e) {
						System.err.println("ERROR Exception parsing line " + line + ": " + e.getMessage());
					}
				}
			} catch (IOException e) {
				System.err.println("ERROR Exception parsing file: " + e.getMessage());
			}
		});
	}

	public Collection<MovieRecord> query(String prefix) {
		if (prefix == null) {
			throw new NullPointerException();
		}
		
		// Sanitize prefix
		prefix = prefix.replaceAll("\\s+", " ").toLowerCase();
		
		if (prefix.isEmpty()) {
			throw new IllegalArgumentException("Search prefix is empty");
		}
		
		return tree.collectWithPrefix(prefix, Collectors.toCollection(TreeSet::new));
	}

	/**
	 * Index the record on all topic parts
	 * For example if tile is: Back to the Future
	 * The record will be indexed in the tree using 4 different indexes:
	 * "Back to the future", "to the Future", "the Future", "Future"
	 * @param rec
	 */
	private void indexRecord(MovieRecord rec) {
		final String title = rec.getTitle();
		final String[] toks = title.split("\\s+");

		if (toks.length > 0) {
			final StringBuilder b = new StringBuilder(toks[toks.length - 1]);
			addRecord(b.toString(), rec);
			for (int i = toks.length - 2; i >= 0; i--) {
				if (!toks[i].isEmpty()) {
					b.insert(0, toks[i] + " ");
					addRecord(b.toString(), rec);
				}
			}
		}	
	}

	private void addRecord(String index, MovieRecord rec) {
		tree.put(index.toLowerCase(), rec);
	}

	@Override
	public void close() throws IOException {
		executor.shutdown(); // Disable new tasks from being submitted
		try {
			// Wait a while for existing tasks to terminate
			if (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
				executor.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!executor.awaitTermination(5, TimeUnit.SECONDS))
					System.err.println("ERROR: Executor did not terminate");
			}
		} catch (InterruptedException ie) {
			// (Re-)Cancel if current thread also interrupted
			executor.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
	}
}
